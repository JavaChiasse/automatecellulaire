import java.util.Iterator;
import java.util.List;
import java.io.*; 
/**
 * 
 * Un automate cellulaire est un objet mathématique qui permet
 * de simuler l'évolution d'une population de cellules virtuelles 
 * au cours du temps, selon des règles de voisinage. Ces regles 
 * sont appliquées à toutes les cellules d'une même génération, 
 * produisant ainsi une nouvelle génération, entièrement 
 * dépendante de la génération précédente. Le << Jeu de la vie >>
 * du mathématicien John Conway est un exemple d'automate cellulaire
 * à deux dimensions dont les règles d’évolution sont les suivantes:
 *
 */
public class JeuDeLaVie implements Observable{
	
	private Cellule[][] plateau;
	private int tailleX, tailleY;
	
	/**
	 * Latence entre chaque génération exprimée en ms
	 */
	private static final int latence = 500;
	
	private List<Observateur> observateurs;
	
	private int generation;
	
	public JeuDeLaVie(){
		
		tailleX = 0;
		tailleY = 0;
		
		tailleX = 50;
		tailleY = 50;
		
		generation = 0;
		
	}
	
	public void initialiserGrille(){
		
		int i = 0, j =0;
		plateau = new Cellule[tailleX][];
		for(i = 0; i < plateau.length; i++)
			plateau[i] = new Cellule[tailleY];
		
		
		for(i = 0; i < tailleX; i ++){
			
			for(j = 0; j < tailleY; j ++){
				
				if((int) (Math.random()* 10) > 5){
		
					plateau[i][j] = new Cellule(i, j, CelluleEtatMort.getInstance());
				}
				
				else{
				
					plateau[i][j] = new Cellule(i, j, CelluleEtatVivant.getInstance());
				}
			}
		}
	}
	
	public Cellule getGrilleXY(int uneCoordX, int uneCoordY) {
		return plateau[uneCoordX][uneCoordY];
	}
	
	public int getTailleX() {
		return tailleX;
	}
	
	public int getTailleY() {
		return tailleY;
	}
	
	public static void main(String args[]) throws InterruptedException {
		
		JeuDeLaVie jeu = new JeuDeLaVie();
		jeu.initialiserGrille();
		
		JeuDeLaVieUI ui = new JeuDeLaVieUI(jeu);
		
		while(true)
		{
			Thread.sleep(latence);
			ui.actualise();
		}
		
	}

	public int getLatence() {
		return latence;
	}
	
	public int getGeneration() {
		return generation;
	}
	
	public void attacheObservateur(Observateur o) {
		observateurs.add(o);
	}

	
	public void detacheObservateur(Observateur o) {
		observateurs.remove(o);
	}
	
	public void notifieObservateurs() {
		
		Iterator<Observateur> it = observateurs.iterator();
		
		while(it.hasNext()){
			
			it.next().actualise();
		}
	}
	

}
