
/**
 * Représente une Cellule
 * @author spi3074
 * @version 0.1
 */
public class Cellule {
	
	private CelluleEtat celluleEtat;
	private int x, y;
	
	/**
	 * Construit l'instance d'une cellule
	 * @param uneCoordX Position X initiale
	 * @param uneCoordY Position Y initiale
	 * @param unEtatInitial Etat initial de la cellule
	 */
	public Cellule(int uneCoordX, int uneCoordY, CelluleEtat unEtatInitial){
		
		this.x = uneCoordX;
		this.y = uneCoordY;
		this.celluleEtat = unEtatInitial;
	}
	
	/**
	 * Fait vivre la cellule
	 */
	public void vit() {
		celluleEtat = celluleEtat.vit();
	}
	
	/**
	 * Fait mourir la cellule
	 */
	public void meurt() {
		celluleEtat = celluleEtat.meurt();
	}
	
	/**
	 * Vérifie l'état de la cellule
	 * @return boolean
	 */
	public boolean estVivante() {
		return celluleEtat.estVivante();
	}
	
	/**
	 * Récupère la coordonnée X de la cellule
	 * @return int
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Récupère la coordonnée X de la cellule
	 * @return int
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Récupère l'état CelluleEtat de la cellule
	 * @return int
	 */
	public CelluleEtat getEtat() {
		return celluleEtat;
	}
	
	public void accepte(Visiteur v){
		
		celluleEtat.accepte(v, this);
		
	}
	
	/**
	 * Calcul le nombre de voisine de la cellule
	 * @param jeu Instance du jeu de la vie cible
	 * @return int
	 */
	public int nombreVoisinesVivante(JeuDeLaVie jeu){
		
		int nbVoisines = 0, i, j;
		
		// x == max
		if(x == jeu.getTailleX()-1){
			
			if(y == jeu.getTailleY()-1){
			
				for(i = -1; i < 1; i ++){
			
					for(j = -1; j < 1; j ++){
				
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
			else if(y == 0){
				
				for(i = -1; i < 1; i ++){
					
					for(j = 0; j < 2; j ++){
						
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
			else {
				
				for(i= -1; i < 1; i ++){
					
					for(j = -1; j < 2; j ++){
						
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
		}
		
		//x == min;
		else if(x == 0){
			
			if(y == jeu.getTailleY()-1){
				
				for(i = 0; i < 2; i ++){
			
					for(j = -1; j < 1; j ++){
				
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
			else if(y == 0){
				
				for(i = 0; i < 2; i ++){
					
					for(j = 0; j < 2; j ++){
						
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
			else {
				
				for(i= 0; i < 2; i ++){
					
					for(j = -1; j < 2; j ++){
						
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
		}
		else {
			if(y == jeu.getTailleY()-1){
				
				for(i = -1; i < 1; i ++){
			
					for(j = -1; j < 1; j ++){
				
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
			else if(y == 0){
				
				for(i = -1; i < 1; i ++){
					
					for(j = 0; j < 2; j ++){
						
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
			else {
				
				for(i= -1; i < 2; i ++){
					
					for(j = -1; j < 2; j ++){
						
						if(jeu.getGrilleXY(x+i, y+j).estVivante())
							nbVoisines ++;
					}
				}
			}
		}
		
		return nbVoisines;
	}
	
}
