
public class CelluleEtatMort implements CelluleEtat {
	
	private static CelluleEtatMort singleIn = new CelluleEtatMort();
	
	private CelluleEtatMort(){}
	
	public static CelluleEtatMort getInstance(){
		
		return singleIn;
	}
	
	public CelluleEtat vit() {
		return CelluleEtatVivant.getInstance();
	}


	public CelluleEtat meurt() {

		return this;
	}


	public boolean estVivante() {

		return false;
	}
	
	public void accepte(Visiteur v, Cellule c) {

		v.visiteCelluleMorte(c);
	}
}
