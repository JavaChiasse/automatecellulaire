
public class CommandeVivre implements Commande {
	
	Cellule cellule;
	
	public CommandeVivre(Cellule uneCelluleCible){
		this.cellule = uneCelluleCible;
	}
	
	public void executer(){
		cellule.vit();
	}

}
