import java.awt.*;
import javax.swing.*;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;

public class JeuDeLaVieUI extends JFrame implements ActionListener, ChangeListener, Observateur {
	
	Random randomGenerator = new Random();
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4436329031580907822L;

	private JeuDeLaVie jeu;
	
	Instant previous, current;
	
	long gap;
	/**
	 * Contient l'ensemble des boutons / actions possibles
	 */
	private JPanel panelPrincipal;
	private JButton quitter, nouveau,playOrStop,nextGeneration;
	private JSlider speed;
	
	private Color rouge = new Color(176,23,31);
	private Color vert = new Color(0,205,102);
	private Color bleu = new Color(113,113,198);
	
	static final int SPEED_MAX = 10;
	static final int SPEED_MIN = 1;
	static final int SPEED_INIT = 10;
	
	public JeuDeLaVieUI(JeuDeLaVie uneInstanceAutomateCellulaire){
		jeu = uneInstanceAutomateCellulaire;
		
		setTitle("TP Automate Cellulaire - L3 SPI");
		setSize(800, 400);
		setResizable(true);
		
		panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		getContentPane().add(panelPrincipal);
		
		speed = new JSlider(JSlider.HORIZONTAL, SPEED_MIN, SPEED_MAX, SPEED_INIT);
		quitter = new JButton("Quitter");
		nouveau = new JButton("Nouveau");
		playOrStop = new JButton("►");
		nextGeneration = new JButton("Génération suivante");
		
		panelPrincipal.add(speed, BorderLayout.SOUTH);
		panelPrincipal.add(nouveau, BorderLayout.SOUTH);
		panelPrincipal.add(playOrStop, BorderLayout.SOUTH);
		panelPrincipal.add(nextGeneration, BorderLayout.SOUTH);
		panelPrincipal.add(quitter, BorderLayout.SOUTH);
		
		speed.addChangeListener(this);
		speed.setMajorTickSpacing(2);
		speed.setMinorTickSpacing(1);
		speed.setPaintTicks(true);
		speed.setPaintLabels(true);
		//champNumeroRegle.addPropertyChangeListener("value", this);
		
		quitter.addActionListener(this);
		nouveau.addActionListener(this);
		playOrStop.addActionListener(this);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		this.setResizable(false);
		speed.setValue(jeu.getLatenceCourante());
		setVisible(true);
	}
	
	public void paint(Graphics g){
		
		super.paint(g);
		
		if ( jeu.isShowRenderTimeOn() ) previous = Instant.now();
		
		int graphicMultiplier=15;
		int tailleBordure =1;
		
		int tailleX=jeu.getTailleX();
		int tailleY=jeu.getTailleY();
		
		int positionXInitial = 10, positionYInitial = 80;
		
		g.translate(250, 0);
		g.setClip(0, 0, tailleX*graphicMultiplier, tailleY*graphicMultiplier);
		//Color noirTransparent = new Color(0,0,0,0.2f);
		//Les couleurs transparentes multiplient le temps de rendu par 100...
		Color gris = new Color(200,200,200);
		Color couleurCases=gris;
		g.setColor(couleurCases);
		
		for(int x = 0; x < tailleX; x++){
			
			for(int y = 0; y < tailleY ; y++){
				
				if( jeu.getPlateau()[x][y].estVivante()){
					
					g.setColor(vert);
					g.fillRect(positionXInitial+x*graphicMultiplier, positionYInitial+y*graphicMultiplier, graphicMultiplier-tailleBordure, graphicMultiplier-tailleBordure);
					
				}else{
					g.setColor(rouge);
					g.fillRect(positionXInitial+x*graphicMultiplier, positionYInitial+y*graphicMultiplier, graphicMultiplier-tailleBordure, graphicMultiplier-tailleBordure);
				}
			}
			
		}
		if ( jeu.isShowRenderTimeOn() ) {
			current = Instant.now();
			if (previous != null) {
			    gap = ChronoUnit.MILLIS.between(previous,current);
			    System.out.println(gap);
			}
		}
	}
	
	public void actualise() {
		repaint();
	}
	
	public void actionPerformed(ActionEvent action) {
		
		Object o = action.getSource();
		
		if (o == quitter){
			this.dispose();
		}else if(o == nouveau){
			jeu.initialiserGrille();
		}else if(o == playOrStop){
			jeu.playOrStop();
			if (jeu.estActif()) {
				playOrStop.setText("❚❚");
			}else{
				playOrStop.setText("►");
			}
		}else if(o == nextGeneration){
			jeu.calculerGenerationSuivante();
			actualise();
		}
		
	}

	public void stateChanged(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        jeu.setSpeedMultiplier( (int)source.getValue() );
	    }
	}
}
