
public class CommandeMourir implements Commande {
	
	Cellule cellule;
	
	public CommandeMourir(Cellule uneCelluleCible) {
		this.cellule = uneCelluleCible;
	}
	
	public void executer(){
		cellule.meurt();
	}
	
}
