import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * Un automate cellulaire est un objet mathématique qui permet
 * de simuler l'évolution d'une population de cellules virtuelles 
 * au cours du temps, selon des règles de voisinage. Ces regles 
 * sont appliquées à toutes les cellules d'une même génération, 
 * produisant ainsi une nouvelle génération, entièrement 
 * dépendante de la génération précédente. Le << Jeu de la vie >>
 * du mathématicien John Conway est un exemple d'automate cellulaire
 * à deux dimensions dont les règles d’évolution sont les suivantes:
 *
 */
public class JeuDeLaVie implements Observable{
	
	private Cellule[][] plateau;

	

	private boolean showRenderTimeOn = false;
	
	private int tailleX, tailleY;
	
	/**
	 * Latence entre chaque génération exprimée en ms
	 */
	private final int latenceInitiale = 1000;
	private int latenceCourante = latenceInitiale;
	
	private List<Observateur> observateurs;
	private List<Commande> commandes;
	
	private Visiteur visiteur;
	
	private int generation;
	private boolean actif;
	
	public JeuDeLaVie(){
		
		observateurs= new ArrayList<Observateur>();
		commandes = new LinkedList<Commande>();
		
		tailleX = 0;
		tailleY = 0;
		
		tailleX = 25;
		tailleY = 25;
		
		generation = 0;
		
	}
	
	/**
	 * Réinitialise le plateau du jeu de la vie
	 * Les cellules sont initialisées de manière aléatoire
	 */
	public void initialiserGrille(){
		
		int i = 0, j =0;
		plateau = new Cellule[tailleX][];
		for(i = 0; i < plateau.length; i++)
			plateau[i] = new Cellule[tailleY];
		
		
		for(i = 0; i < tailleX; i ++){
			
			for(j = 0; j < tailleY; j ++){
				
				if((int) (Math.random() * 10) > 5){
		
					plateau[i][j] = new Cellule(i, j, CelluleEtatMort.getInstance());
				}
				
				else{
				
					plateau[i][j] = new Cellule(i, j, CelluleEtatVivant.getInstance());
				}
			}
		}
	}
	/**
	 * Récupère le plateau du jeu
	 * @return Cellule[][]
	 */
	public Cellule[][] getPlateau() {
		return plateau;
	}
	/**
	 * Récupère une cellule dans le plateau à l'indice X,Y
	 * @param uneCoordX
	 * @param uneCoordY
	 * @return Cellule
	 */
	public Cellule getGrilleXY(int uneCoordX, int uneCoordY) {
		return plateau[uneCoordX][uneCoordY];
	}
	
	/**
	 * Remplace une cellule par une nouvelle définie en paramètre
	 * @param uneNouvelleCellule
	 */
	public void setGrilleXY(Cellule uneNouvelleCellule) {
		plateau[uneNouvelleCellule.getX()][uneNouvelleCellule.getY()] = uneNouvelleCellule;
	}
	/**
	 * Récupère la dimension X du plateau
	 * @return int
	 */
	public int getTailleX() {
		return tailleX;
	}
	/**
	 * Récupère la dimension Y du plateau
	 * @return int
	 */
	public int getTailleY() {
		return tailleY;
	}
	
	public static void main(String args[]) throws InterruptedException {
		
		JeuDeLaVie jeu = new JeuDeLaVie();
		jeu.initialiserGrille();
		
		JeuDeLaVieUI ui = new JeuDeLaVieUI(jeu);
		
		jeu.attacheObservateur(ui);
		jeu.notifieObservateurs();
		
		jeu.visiteur = new VisiteurClassique(jeu);
		
		jeu.setSpeedMultiplier(10);
		
		while(true){
			Thread.sleep(jeu.getLatenceCourante());
			if (jeu.estActif())
				jeu.calculerGenerationSuivante();
		}
		
	}
	
	/**
	 * Vérifie si le jeu génére les générations suivantes automatiquements
	 * @return boolean
	 */
	public boolean estActif() {
		return actif;
	}
	
	/**
	 * Récupère la latence entre deux frames
	 * @return int
	 */
	public int getLatenceCourante() {
		return latenceCourante;
	}
	
	/**
	 * Récupère le numéro de génération
	 * @return int
	 */
	public int getGeneration() {
		return generation;
	}
	
	public void attacheObservateur(Observateur o) {
		observateurs.add(o);
	}

	
	public void detacheObservateur(Observateur o) {
		observateurs.remove(o);
	}
	
	public void notifieObservateurs() {
		
		Iterator<Observateur> it = observateurs.iterator();
		
		while(it.hasNext()){
			
			it.next().actualise();
		}
	}

	public void ajouteCommande(Commande c){
		commandes.add(c);
	}
	
	public void executerCommandes(){
		
		Iterator<Commande> it = commandes.iterator();
		
		while(it.hasNext()){
			
			it.next().executer();
		}
		commandes.clear();
	}
	
	public void distribueVisiteurs(){
		
		int x, y;
		
		for(x = 0; x < tailleX; x ++){
			
			for(y = 0; y < tailleY; y ++){
				
				plateau[x][y].accepte(visiteur);
				
			}
		}		
	}
	
	/**
	 * Calcul et rafraichit le plateau pour la génération suivante
	 */
	public void calculerGenerationSuivante(){
		
		this.distribueVisiteurs();
		this.executerCommandes();
		this.notifieObservateurs();
		this.generation++;
		
	}
	/**
	 * Arrête ou relance la génération automatique
	 */
	public void playOrStop() {
		actif = !actif;
	}
	
	/**
	 * Affecte une nouvelle latence
	 * @param value Centième de seconde
	 */
	public void setSpeedMultiplier(int value) {
		latenceCourante = value*100;
	}
	
	public boolean isShowRenderTimeOn() {
		return showRenderTimeOn;
	}

	public void setShowRenderTimeOn(boolean showRenderTimeOn) {
		this.showRenderTimeOn = showRenderTimeOn;
	}



}
