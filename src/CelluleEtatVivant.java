
public class CelluleEtatVivant implements CelluleEtat {
	
	private static CelluleEtatVivant singleIn = new CelluleEtatVivant();
	
	private CelluleEtatVivant(){}
	
	public static CelluleEtatVivant getInstance(){
		
		return singleIn;
	}

	public CelluleEtat vit() {
	
		return this;
	
	}

	public CelluleEtat meurt() {
		
		return CelluleEtatMort.getInstance();
	}

	public boolean estVivante() {

		return true;
	}
	
	public void accepte(Visiteur v, Cellule c) {

		v.visiteCelluleVivante(c);
	}
}
