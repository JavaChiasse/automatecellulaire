public abstract class Visiteur {

	JeuDeLaVie jeu;
	
	public Visiteur(JeuDeLaVie j){
		
		this.jeu = j;
	}
	
	public void visiteCelluleVivante(Cellule c){}
	
	public void visiteCelluleMorte(Cellule c){}
	
}
