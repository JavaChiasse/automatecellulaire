

public class VisiteurClassique extends Visiteur {

	public VisiteurClassique(JeuDeLaVie j) {
		super(j);
	}
	
	public void visiteCelluleVivante(Cellule c){
		
		int nbVoisines = c.nombreVoisinesVivante(jeu);
		
		if((nbVoisines > 3 || nbVoisines < 2) && (nbVoisines != 2)){
			
			jeu.ajouteCommande(new CommandeMourir(c));
			
		}else{
			
			jeu.ajouteCommande(new CommandeVivre(c));
			
		}
	}
	
	public void visiteCelluleMorte(Cellule c){
		
		int nbVoisines = c.nombreVoisinesVivante(jeu);
		
		if(nbVoisines == 3){
			
			jeu.ajouteCommande(new CommandeVivre(c));
			
		}else if(nbVoisines != 2){
			
			jeu.ajouteCommande(new CommandeMourir(c));
			
		}
	}

}